# Bakery

Bakery is an extremely simple build scripting language. It's intended to give programmers more explicit control over
what their build is doing than something like a Makefile, while still remaining straightforward and easy to learn.

## Usage 

Bakery is invoked using the `bake` command. It has no command line options or special arguments, other than a list of recipes:
```
bake debug.recipe
```
Recipes are script files that Bakery uses to build your project. You can provide them by simply providing the filename, as above.

If you omit the `.recipe` suffix, Bakery will look for a `.recipe` file with that name in the same directory:
```
bake debug
```
If no file is found, Bakery will also look for a folder named `recipes` in the open directory and see if it can find the recipe in
there.

Bakery will accept multiple recipes as arguments, as noted above. The behavior is equivalent to invoking `bake` on each of the
arguments independently. For example, this:
```
bake clean debug
```
is equivalent to writing:
```
bake clean.recipe
bake debug.recipe
```

## Script Documentation

### Types

Bakery is dynamically and strongly typed. There are three types generally available:
 * Strings. These represent words, text, file paths, etc.
 * Lists. These can contain an arbitrary number of other objects.
 * Mappings. These are essentially just lists of pairs, but they are useful for a few steps of building a project.

String values can be provided as literals. Any quote-enclosed string of text will be used as a string value:
```
# Examples of strings.
"Hello world"
"Test"
"src/Main.cpp"
```

Lists can be assembled using parentheses. Any sequence of space-separated values enclosed in parentheses is a list. Lists
can also include other lists, to allow for complex data arrangement:
```
# Examples of lists.
("a" "b" "c")
("hello" "world")
("weekdays" ("mon" "tue" "wed" "thu" "fri"))
```

Mappings are constructed using the `on` operator. The `on` operator requires a function on the left hand side and a list on the right.
We'll discuss it more shortly.

### Variables

Variables are declared using the `var` keyword:
```
var x = "test"
```
Once a variable is declared, it can be accessed by its by name, and be used wherever values are expected:
```
var y = x
var list = (x y "z")
```
Variables declared outside of code blocks are in the global scope of your script and can be accessed anywhere after they are declared. Variables declared within a `do ... end` block are local only to that block and any descendant scopes of that block.

### Control Flow

There are two control flow constructs in Bakery: the `for` loop and the `if` statement.

The `if` statement is followed by a `do ... end` block, and will execute that block if and only if its condition is true. Currently, since booleans are not a type in the language, this necessitates the use of some kind of built-in test function:
```
if newer("a.txt", "b.txt") do
    print "Test"
end
```
We'll cover built-in functions in a bit.

The `for` loop will repeatedly execute its `do ... end` block, once for each element in a container. It also creates one or more variables that store the values of elements in the container during these iterations. For instance:
```
for str in ("a" "b" "c") do
    print str
end

for old new in swap("a" "b") on ("a") do
    print old
    print new
    print ""
end
```
`for` loops can iterate over compound types, specifically referring to the list and mapping types. For a mapping, note that two iterating variables need to be declared, not just one.

### Statements

There are three top-level statements that can be executed in a Bakery script: `print`, `run`, and `set`.

`print` is fairly straightforward - it will print whatever value it is provided to standard output.
```
# Prints "Hello world" without the quotes.
print "Hello world"

# Prints "(a b c)" without the quotes.
print ("a" "b" "c")
```

`run` is used to execute a command in your system's shell. It accepts a list of strings, representing the command and arguments you wish to run:
```
var src = "folder1"
var dest = "folder2"
run ("cp" "-r" file dir)
```

`set` is used to set the value of a variable after it has already been declared:
```
var filename = "oldfile.txt"
set filename to "newfile.txt"
```
The `to` is required as part of the script syntax.

### Transformations

Bakery contains a few built-in functions for use in creating mappings. Mappings are created with the use of a function applied to a list. The result is a mapping object, with the set of inputs being the original list and the set of outputs having the function applied to them.

`swap` allows you to replace substrings within strings. Its argument list must contain two elements: a string or list of strings representing "source" substrings, and then a string to replace any of the "source" substrings that get found:
```
var words = ("apple" "ball" "cat" "dingo")
var replace = swap(("a" "b") "c") on words

# ("cpple" "ccll" "cct" "dingo")
var out = outputs(replace)
```

### Filters

Bakery can filter the contents of a list using a built-in filter function and the `with` keyword. Use this to remove certain values from a list that don't satisfy the function. Using a filter will not modify the original list, instead creating a new list with a subset of the original elements.

`ending` will only accept strings that end with a certain other string. It accepts an argument string or list, representing the possible endings it will accept:
```
var contents = ("a.txt" "b.txt" "c.doc" "d.docx")
var docs = contents with ending(".doc" ".docx")
```

### Expressions

Bakery contains a number of other built-in functions that can transform or process different types of data. You can use these functions whenever a value is expected.

`expand` accepts a list, and returns a "flattened" version of that list. Any nested lists the original list contained will be removed, with their contents added to the return list:
```
# ("a" "b" "c")
print expand("a" ("b" "c")) 
```

`files` accepts an argument list of strings, each representing a directory to search. The function returns a list of file names representing all of the files in the directories, recursively searching them. The list does not contain nested lists and does not reflect the folder structure the function traverses, but each file name does contain the folders Bakery had to search through o find it. Before `files` is executed, its argument list will be `expand`ed.
```
var sources = files("src")
var data = files("data_2017" "data_2018" "data_2019") with ending(".csv")
```

`concat` accepts a list and returns a single string, the result of chaining all of the individual strings in the list together in order. Before `concat` is executed, its argument list will be `expand`ed.
```
var hello = "hello"
var space = " "
var world = "world"
print concat(hello space world)
```

## Example

You can find examples of Bakery syntax in this repository, specifically in `debug.recipe` and `clean.recipe`.

Thanks for taking a look! This tool is not very formalized yet, the lexical and syntax analysis is very touch-and-go, so it may be unstable. But I've found it very useful so far.