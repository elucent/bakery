#include "Bakery.h"

using namespace std;
using namespace std::experimental::filesystem;
using namespace bake;

string FindRecipe(const string& location) {
    if (exists(location) && !is_directory(location)) return location;
    else if (exists(location + ".recipe") && is_regular_file(location + ".recipe")) return location + ".recipe";
    else if (exists("recipes") && is_directory("recipes")) return FindRecipe(string("recipes/") + location);
    else return "";
}

int main(int argc, char** argv) {
    for (int i = 1; i < argc; i ++) {
        ifstream in;
        in.open(FindRecipe(argv[i]));

        try {
            if (!in) throw RuntimeError(0, string("Unable to open recipe '") + string(argv[1]) + string("' (resolved to '") + FindRecipe(argv[1]) + "').");
            vector<Token> tokens = Lex(in);

            Context ctx(tokens);
            ctx.process();
        }
        catch (exception& e) {
            cout << e.what() << endl;
            return 1;
        }
        in.close();
    }

    return 0;
}