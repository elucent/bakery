#include "Bakery.h"

namespace bake {
    using namespace std;

    // Lexical

    unordered_map<string, TokenType> KEYWORDS = {
        { "var", TokenType::VAR },
        { "set", TokenType::SET },
        { "target", TokenType::TARGET },
        { "swap", TokenType::SWAP },
        { "files", TokenType::FILES },
        { "concat", TokenType::CONCAT },
        { "ending", TokenType::ENDING },
        { "newer", TokenType::NEWER },
        { "inputs", TokenType::INPUTS },
        { "outputs", TokenType::OUTPUTS },
        { "expand", TokenType::EXPAND },
        { "print", TokenType::PRINT },
        { "run", TokenType::RUN },
        { "for", TokenType::FOR },
        { "if", TokenType::IF },
        { "do", TokenType::DO },
        { "end", TokenType::END },  
        { "with", TokenType::WITH },
        { "on", TokenType::ON },
        { "in", TokenType::IN },
        { "to", TokenType::TO }  
    };

    unordered_map<char, TokenType> SYMBOLS = {
        { '(', TokenType::LPAREN },
        { ')', TokenType::RPAREN },
        { '=', TokenType::ASSIGN }
    };

    LexicalError::LexicalError(int line, const string& message_in) 
        : message(string("[LEXICAL ERROR] Line ") + to_string(line) + string(": ") + message_in) {
        //
    }

    const char* LexicalError::what() const noexcept {
        return message.c_str();
    }

    vector<Token> Lex(istream& is) {
        vector<Token> tokens;
        char ch = 0;
        int line = 1;
        string name, str;
        while (is.rdbuf()->in_avail()) {
            ch = is.get();

            if (str.size()) {
                if (ch == '"' && str.back() != '\\') {
                    tokens.push_back({ TokenType::STRING, str.substr(1, str.size()-1) });
                    str = "";
                }
                else if (ch == '\n') {
                    throw LexicalError(line, string("Unterminated string literal '") + str.substr(1) + string("'."));
                }
                else str += ch;
            }
            else if (isalpha(ch) || (isalnum(ch) && name.size())) {
                name += ch;
            }
            else {
                if (name.size()) {
                    if (KEYWORDS.find(name) != KEYWORDS.end()) tokens.push_back({ KEYWORDS[name], name });
                    else tokens.push_back({ TokenType::IDENTIFIER, name });
                    name = "";
                }

                if (ch == '"') {
                    str += ch;
                }
                else if (ch == '\n') {
                    tokens.push_back({ TokenType::LINE, "\n" });
                    line ++;
                }
                else if (SYMBOLS.find(ch) != SYMBOLS.end()) {
                    tokens.push_back({ SYMBOLS[ch], string() + ch });
                }
                else if (!isspace(ch)) {
                    throw LexicalError(line, string("Unexpected char '") + ch + string("' in input."));
                }
            }
        }
        if (name.size()) {
            if (KEYWORDS.find(name) != KEYWORDS.end()) tokens.push_back({ KEYWORDS[name], name });
            else tokens.push_back({ TokenType::IDENTIFIER, name });
            name = "";
        }
        if (str.size()) throw LexicalError(line, string("Unterminated string literal '") + str.substr(1) + string("'."));
        
        return tokens;
    }

    // General

    const char* GetTypename(Type type) {
        switch (type) {
            case Type::STRING:
                return "STRING";
            case Type::LIST:
                return "LIST";
            case Type::MAPPING:
                return "MAPPING";
        }
    }

    RuntimeError::RuntimeError(int line, const string& message_in) 
        : message(string("[RUNTIME ERROR] Line ") + to_string(line) + string(": ") + message_in) {
        //
    }

    const char* RuntimeError::what() const noexcept {
        return message.c_str();
    }

    void Context::concat(Variable* target, Variable* other) {
        if (other->getType() == Type::STRING) target->getString() += other->getString();
        else if (other->getType() == Type::LIST) for (Variable* v : other->getList()) concat(target, v);
        else if (other->getType() == Type::MAPPING) throw RuntimeError(line, string("Attempted to concatenate variable of type ") + GetTypename(Type::MAPPING) + string("."));
    }

    Variable* Context::files(Variable* folders) {
        using namespace experimental::filesystem;

        vector<string> dirs;
        for (Variable* s : folders->getList()) dirs.push_back(s->getString());

        unordered_set<string> filenames;
        while (!dirs.empty()) {
            auto it = directory_iterator(dirs.back());
            dirs.pop_back();
            for (const auto& entry : it) {
                if (is_directory(entry)) dirs.push_back(entry.path());
                else if (is_regular_file(entry)) filenames.insert(entry.path());
            }
        }

        Variable* list = new Variable(*this, ListType());
        for (auto &s : filenames) list->getList().push_back(new Variable(*this, s));
        return list;
    }

    bool endswith(const string& a, const string& b) {
        if (a.size() < b.size()) return false;
        return a.substr(a.size() - b.size()) == b;
    }

    void expand(Variable* list, Variable* v) {
        if (v->getType() == Type::STRING) list->getList().push_back(new Variable(*v, ""));
        else if (v->getType() == Type::LIST) for (Variable* e : v->getList()) expand(list, e);
        else if (v->getType() == Type::MAPPING) for (auto &p : v->getMap()) expand(list, p.first), expand(list, p.second);
    }

    Variable* Context::filterValue(Variable* v) {
        Variable* result = nullptr;

        if (tokens[t].type == TokenType::ENDING) {
            if (v->getType() != Type::LIST) throw RuntimeError(line, string("Built-in function 'ending' can only act on ") + GetTypename(Type::LIST) + string(" values, found ") + GetTypename(v->getType()) + string(" instead."));
            t ++;
            
            result = new Variable(*this, ListType());
            Variable* o = parseValue();
            if (o->getType() != Type::LIST) throw RuntimeError(line, "Expected argument list for built-in function 'ending'.");
            Variable* list = new Variable(*this, ListType());
            expand(list, o);

            for (Variable* element : v->getList()) {
                for (Variable* end : list->getList()) {
                    if (end->getType() != Type::STRING) throw RuntimeError(line, string("Built-in function 'ending' expects strings as arguments, found value of type ") + GetTypename(end->getType()) + string(" instead."));
                    if (endswith(element->getString(), end->getString())) {
                        result->getList().push_back(new Variable(*element, ""));
                        break;
                    }
                }
            }

            if (list->isTemporary()) delete list;
        }

        if (v->isTemporary()) delete v;
        return result;
    }

    Variable* Context::parseValue(bool single) {
        Variable* result;
        if (tokens[t].type == TokenType::STRING) {
            result = new Variable(*this, tokens[t ++].value);
        }
        else if (tokens[t].type == TokenType::IDENTIFIER) {
            auto it = variables.find(tokens[t].value);
            if (it == variables.end()) throw RuntimeError(line, string("Use of unknown variable '") + tokens[t].value + string("'."));
            t ++;
            result = it->second;
        }
        else if (tokens[t].type == TokenType::LPAREN) {
            t ++; // Consume left paren.
            Variable* v = new Variable(*this, ListType());
            while (tokens[t].type != TokenType::RPAREN) {
                Variable* temp = parseValue();
                v->getList().push_back(new Variable(*temp, ""));
                if (temp->isTemporary()) delete temp;
            }
            t ++; // Consume right paren.
            result = v;
        }
        else if (tokens[t].type == TokenType::CONCAT) {
            t ++; // Consume concat.
            Variable* v = new Variable(*this, "");
            Variable* o = parseValue(true);
            if (o->getType() != Type::LIST) throw RuntimeError(line, "Expected argument list for built-in function 'concat'.");
            concat(v, o);
            if (o->isTemporary()) delete o;
            result = v;
        }
        else if (tokens[t].type == TokenType::FILES) {
            t ++; // Consume concat.
            Variable* o = parseValue(true);
            if (o->getType() != Type::LIST) throw RuntimeError(line, "Expected argument list for built-in function 'files'.");
            Variable* args = new Variable(*this, ListType());
            expand(args, o);
            Variable* v = files(args);
            if (o->isTemporary()) delete o;
            delete args;
            result = v;
        }
        else if (tokens[t].type == TokenType::EXPAND) {
            t ++;
            Variable* o = parseValue(true);
            Variable* v = new Variable(*this, ListType());
            expand(v, o);
            if (o->isTemporary()) delete o;
            result = v;
        }
        else if (tokens[t].type == TokenType::SWAP) {
            t ++;
            Variable* o = parseValue(true);
            if (o->getType() != Type::LIST) throw RuntimeError(line, "Expected argument list for built-in function 'swap'.");
            
            if (tokens[t].type != TokenType::ON) throw RuntimeError(line, "Expected function 'swap' to be applied to a value. (Maybe you missed the 'on' keyword?)");
            t ++;
            Variable* src = parseValue();
            if (src->getType() != Type::LIST) throw RuntimeError(line, string("Expected function 'swap' to be applied to value of type ") + GetTypename(Type::LIST) + string(", got ") + GetTypename(src->getType()) + string(" instead."));
        
            Variable* m = new Variable(*this, MappingType());
            
            vector<string> from;
            Variable* targets = o->getList()[0];
            if (targets->getType() == Type::STRING) from.push_back(targets->getString());
            else if (targets->getType() == Type::LIST) for (Variable* str : targets->getList()) {
                if (find(from.begin(), from.end(), str->getString()) == from.end())
                    from.push_back(str->getString());
            }
            string to = o->getList()[1]->getString();

            for (Variable* v : src->getList()) {
                Variable* in = new Variable(*v, "");
                Variable* out = new Variable(*v, "");
                for (const string& s : from) {
                    size_t i = out->getString().find(s);
                    if (i != string::npos) {
                        out->getString().replace(i, s.size(), to);
                    }
                }
                m->getMap().push_back({ in, out });
            }

            result = m;
        }
        else if (tokens[t].type == TokenType::INPUTS) {
            t ++;
            Variable* o = parseValue(true);
            if (o->getType() != Type::LIST) throw RuntimeError(line, "Expected argument list for built-in function 'inputs'.");
            Variable* l = new Variable(*this, ListType());
            for (Variable* v : o->getList()) {
                if (v->getType() != Type::MAPPING) throw RuntimeError(line, string("Expected value of type '") + GetTypename(Type::MAPPING) + string("' for built-in function 'inputs'."));
                for (auto &p : v->getMap()) l->getList().push_back(new Variable(*p.first));
            }
            if (o->isTemporary()) delete o;
            result = l;
        }
        else if (tokens[t].type == TokenType::OUTPUTS) {
            t ++;
            Variable* o = parseValue(true);
            if (o->getType() != Type::LIST) throw RuntimeError(line, "Expected argument list for built-in function 'outputs'.");
            Variable* l = new Variable(*this, ListType());
            for (Variable* v : o->getList()) {
                if (v->getType() != Type::MAPPING) throw RuntimeError(line, string("Expected value of type '") + GetTypename(Type::MAPPING) + string("' for built-in function 'outputs'."));
                for (auto &p : v->getMap()) l->getList().push_back(new Variable(*p.second));
            }
            if (o->isTemporary()) delete o;
            result = l;
        }
        else throw RuntimeError(line, string("Encountered unexpected token '") + tokens[t].value + string("'."));

        if (!single) while (tokens[t].type == TokenType::WITH) {
            t ++; // Consume with.
            result = filterValue(result);
        }
        return result;
    }

    void Context::print() {
        t ++;
        Variable* o = parseValue(true);
        Variable* l = new Variable(*this, ListType());
        expand(l, o);
        for (Variable* v : l->getList()) cout << v->getString();
        cout << endl;

        if (o->isTemporary()) delete o;
        delete l;
    }

    void Context::run() {
        Variable* v = parseValue(true);
        Variable* args = new Variable(*this, ListType());
        expand(args, v);
        string cmd = "";
        for (Variable* s : args->getList()) cmd += s->getString(), cmd += " ";
        cout << cmd << endl;
        system(cmd.c_str());
        delete args;
        if (v->isTemporary()) delete v;
    }

    void Context::addVariable() {
        if (tokens[t].type != TokenType::IDENTIFIER) throw RuntimeError(line, "Expected identifier in variable declaration.");
        string name = tokens[t].value;
        t ++;

        if (tokens[t].type != TokenType::ASSIGN) throw RuntimeError(line, "Expected initializer in variable declaration.");
        
        t ++;
        Variable* v = parseValue();
        variables.insert({ name, new Variable(*v, name) });
        if (v->isTemporary()) delete v;
    }

    void Context::doLoop() {
        vector<string> vars;
        while (tokens[t].type != TokenType::IN) {
            if (tokens[t].type != TokenType::IDENTIFIER) throw RuntimeError(line, "Expected variable declaration in for loop.");
            vars.push_back(tokens[t].value);
            t ++;
        }
        if (vars.empty()) throw RuntimeError(line, "For loop requires at least one variable for iteration.");
        
        t ++; // Consume in.
        Variable* o = parseValue();
        if (o->getType() == Type::STRING) throw RuntimeError(line, string("Cannot iterate over value of type ") + GetTypename(Type::STRING) + ".");
        if (o->getType() == Type::LIST && vars.size() != 1) throw RuntimeError(line, string("Expected single iteration variable for value of type ") + GetTypename(Type::LIST) + ", found multiple instead.");
        if (o->getType() == Type::MAPPING && vars.size() != 2) throw RuntimeError(line, string("Expected two iteration variables for value of type ") + GetTypename(Type::MAPPING) + ".");

        int size = o->getType() == Type::LIST ? o->getList().size() : o->getMap().size();
        int elt = 0;

        if (tokens[t].type != TokenType::DO) throw RuntimeError(line, "Expected keyword 'do' after for loop declaration.");
        t ++; // Consume do.

        int start = t;
        int startln = line;

        if (o->getType() == Type::LIST) variables.insert({ vars[0], new Variable(*o->getList()[0], vars[0]) });
        if (o->getType() == Type::MAPPING) variables.insert({ vars[0], new Variable(*o->getMap()[0].first, vars[0]) }), variables.insert({ vars[1], new Variable(*o->getMap()[0].second, vars[1]) });

        while (elt < size) {
            if (tokens[t].type == TokenType::END) {
                elt ++;
                if (elt >= size) break;
                t = start;
                line = startln;
                if (o->getType() == Type::LIST) delete variables[vars[0]], variables[vars[elt]] = new Variable(*o->getList()[elt], vars[0]);
                if (o->getType() == Type::MAPPING) delete variables[vars[0]], variables[vars[1]], variables[vars[0]] = new Variable(*o->getMap()[elt].first, vars[0]), variables[vars[1]] = new Variable(*o->getMap()[elt].second, vars[1]);
            }
            else processLine();
        }

        for (const string& v : vars) variables.erase(v);

        if (o->isTemporary()) delete o;
    }

    bool Context::check() {
        bool result = false;
        if (tokens[t].type == TokenType::NEWER) {
            t ++; // Consume newer.
            
            Variable* o = parseValue(true);
            if (o->getType() != Type::LIST) throw RuntimeError(line, "Expected argument list for built-in function 'newer'.");
            if (o->getList().size() != 2) throw RuntimeError(line, string("Expected two elements in argument list for 'newer', found ") + to_string(o->getList().size()) + string("."));
            const string &first = o->getList()[0]->getString(), &second = o->getList()[1]->getString();

            if (!experimental::filesystem::exists(first)) throw RuntimeError(line, string("Could not open file '") + first + "'.");
            if (experimental::filesystem::exists(second))
                result = experimental::filesystem::last_write_time(first) > experimental::filesystem::last_write_time(second);
            else result = true;
            if (o->isTemporary()) delete o;
        }
        else throw RuntimeError(line, string("Encountered unexpected token '") + tokens[t].value + string("'."));
        return result;
    }

    void Context::doIf() {
        bool b = check();
        if (tokens[t].type != TokenType::DO) throw RuntimeError(line, "Expected keyword 'do' after if statement.");
        t ++; // Consume do.

        if (b) while (tokens[t].type != TokenType::END) processLine();
        else {
            int num = 0;
            while (num != 0 || tokens[t].type != TokenType::END) {
                if (tokens[t].type == TokenType::IF || tokens[t].type == TokenType::FOR) num ++;
                else if (tokens[t].type == TokenType::END) num --;
                if (t >= tokens.size()) throw RuntimeError(line, "Unterminated if statement.");
                skipLine();
            }
        }

        t ++; // Consume end.
    }

    void Context::setVariable() {
        if (tokens[t].type != TokenType::IDENTIFIER) throw RuntimeError(line, string("Expected identifier after keyword 'set', found '") + tokens[t].value + string("' instead."));
        auto it = variables.find(tokens[t].value);
        if (it == variables.end()) throw RuntimeError(line, string("Could not find variable '") + tokens[t].value + "'.");
        Variable* v = it->second;
        t ++; // Consume identifier.
        
        if (tokens[t].type != TokenType::TO) throw RuntimeError(line, "Expected keyword 'to' in 'set' statement.");
        t ++; // Consume to.

        Variable* o = parseValue();
        *v = *o;

        if (o->isTemporary()) delete o;
    }

    void Context::processLine() {
        switch (tokens[t ++].type) {
            case TokenType::FOR:
                doLoop();
                break;
            case TokenType::VAR:
                addVariable();
                break;
            case TokenType::LINE:
                line ++;
                break;
            case TokenType::SET:
                setVariable();
                break;
            case TokenType::RUN:
                run();
                break;
            case TokenType::IF:
                doIf();
                break;
            case TokenType::PRINT:
                print();
                break;
            default:
                break;
        }
    }

    void Context::skipLine() {
        switch (tokens[t ++].type) {
            case TokenType::LINE:
                line ++;
                break;
            default:
                break;
        }
    }

    Context::Context(const vector<Token>& tokens_in) 
        : line(1), t(0), tokens(tokens_in) {
        //
    }

    Context::~Context() {
        for (auto p : variables) delete p.second;
    }

    int Context::getLine() const {
        return line;
    }

    void Context::process() {
        while (t < tokens.size()) {
            processLine();
        }
    }

    Variable::Variable(Context& ctx_in, const string& value, const string& name_in) 
        : ctx(ctx_in), name(name_in), type(Type::STRING), data((void*)new string(value)){
        //
    }

    Variable::Variable(Context& ctx_in, const ListType& values, const string& name_in) 
        : ctx(ctx_in), name(name_in), type(Type::LIST), data((void*)new ListType(values)){
        //
    }
    
    Variable::Variable(Context& ctx_in, const MappingType& mappings, const string& name_in) 
        : ctx(ctx_in), name(name_in), type(Type::MAPPING), data((void*)new MappingType(mappings)) {
        //
    }
    
    Variable::Variable(Context& ctx_in, Type type_in, const string& name_in) 
        : ctx(ctx_in), name(name_in), type(type_in), data(nullptr) {
        //
    }
    
    Variable::Variable(const Variable& other)   
        : ctx(other.ctx), name(""), type(other.type) {
        switch (type) {
            case Type::STRING:
                data = (void*)new string(*(string*)other.data);
                break;
            case Type::LIST:
                data = (void*)new ListType();
                for (Variable* v : *(ListType*)other.data) ((ListType*)data)->push_back(new Variable(*v));
                break;
            case Type::MAPPING:
                data = (void*)new MappingType();
                for (auto &p : *(MappingType*)other.data) ((MappingType*)data)->push_back({ new Variable(*p.first), new Variable(*p.second) });
                break;
        }
    }
    
    Variable::Variable(const Variable& other, const string& name_in)   
        : Variable(other) {
        name = name_in;
    }
    
    Variable& Variable::operator=(const Variable& other) {
        if (this != &other) {
            switch (type) {
                case Type::STRING:
                    delete (string*)data;
                    break;
                case Type::LIST:
                    for (Variable* v : *(ListType*)data) if (v->isTemporary()) delete v;
                    delete (ListType*)data;
                    break;
                case Type::MAPPING:
                    for (auto &p : *(MappingType*)data) {
                        if (p.first->isTemporary()) delete p.first;
                        if (p.second->isTemporary()) delete p.second;
                    }
                    delete (MappingType*)data;
                    break;
            }

            type = other.type;

            switch (type) {
                case Type::STRING:
                    data = (void*)new string(*(string*)other.data);
                    break;
                case Type::LIST:
                    data = (void*)new ListType();
                    for (Variable* v : *(ListType*)other.data) ((ListType*)data)->push_back(new Variable(*v));
                    break;
                case Type::MAPPING:
                    data = (void*)new MappingType();
                    for (auto &p : *(MappingType*)other.data) ((MappingType*)data)->push_back({ new Variable(*p.first), new Variable(*p.second) });
                    break;
            }
        }
        return *this;
    }
    
    Variable::~Variable() {
        switch (type) {
            case Type::STRING:
                delete (string*)data;
                break;
            case Type::LIST:
                for (Variable* v : *(ListType*)data) if (v->isTemporary()) delete v;
                delete (ListType*)data;
                break;
            case Type::MAPPING:
                for (auto &p : *(MappingType*)data) {
                    if (p.first->isTemporary()) delete p.first;
                    if (p.second->isTemporary()) delete p.second;
                }
                delete (MappingType*)data;
                break;
        }
    }

    bool Variable::isTemporary() const {
        return name.empty();
    }

    const string& Variable::getName() const {
        return name;
    }
    
    Type Variable::getType() const {
        return type;
    }
    
    const string& Variable::getString() const {
        if (!data) throw RuntimeError(ctx.getLine(), string("Attempted to read variable '") + getName() + string("' which has value 'null'!"));
        if (type != Type::STRING) throw RuntimeError(ctx.getLine(), string("Attempted to read ") + GetTypename(Type::STRING) + string(" value from variable '") + name + string("', which has type ") + GetTypename(type) + ".");
        return *(const string*)data;
    }
    
    string& Variable::getString() {
        if (!data) throw RuntimeError(ctx.getLine(), string("Attempted to read variable '") + getName() + string("' which has value 'null'!"));
        if (type != Type::STRING) throw RuntimeError(ctx.getLine(), string("Attempted to read ") + GetTypename(Type::STRING) + string(" value from variable '") + name + string("', which has type ") + GetTypename(type) + ".");
        return *(string*)data;
    }
    
    const ListType& Variable::getList() const {
        if (!data) throw RuntimeError(ctx.getLine(), string("Attempted to read variable '") + getName() + string("' which has value 'null'!"));
        if (type != Type::LIST) throw RuntimeError(ctx.getLine(), string("Attempted to read ") + GetTypename(Type::LIST) + string(" value from variable '") + name + string("', which has type ") + GetTypename(type) + ".");
        return *(const ListType*)data;
    }
    
    ListType& Variable::getList() {
        if (!data) throw RuntimeError(ctx.getLine(), string("Attempted to read variable '") + getName() + string("' which has value 'null'!"));
        if (type != Type::LIST) throw RuntimeError(ctx.getLine(), string("Attempted to read ") + GetTypename(Type::LIST) + string(" value from variable '") + name + string("', which has type ") + GetTypename(type) + ".");
        return *(ListType*)data;
    }
    
    const MappingType& Variable::getMap() const {
        if (!data) throw RuntimeError(ctx.getLine(), string("Attempted to read variable '") + getName() + string("' which has value 'null'!"));
        if (type != Type::MAPPING) throw RuntimeError(ctx.getLine(), string("Attempted to read ") + GetTypename(Type::MAPPING) + string(" value from variable '") + name + string("', which has type ") + GetTypename(type) + ".");
        return *(const MappingType*)data;
    }
    
    MappingType& Variable::getMap() {
        if (!data) throw RuntimeError(ctx.getLine(), string("Attempted to read variable '") + getName() + string("' which has value 'null'!"));
        if (type != Type::MAPPING) throw RuntimeError(ctx.getLine(), string("Attempted to read ") + GetTypename(Type::MAPPING) + string(" value from variable '") + name + string("', which has type ") + GetTypename(type) + ".");
        return *(MappingType*)data;
    }

    ostream& operator<<(ostream& os, const Variable& var) {
        switch (var.getType()) {
            case Type::STRING:
                return os << var.getString();
            case Type::LIST:
                os << "(";
                for (size_t i = 0; i < var.getList().size(); i ++) {
                    os << *var.getList()[i];
                    if (i != var.getList().size() - 1) os << " ";
                }
                return os << ")";
            case Type::MAPPING:
                os << "(";
                for (size_t i = 0; i < var.getMap().size(); i ++) {
                    os << "(" << *var.getMap()[i].first << " -> " << *var.getMap()[i].second << ")";
                    if (i != var.getMap().size() - 1) os << " ";
                }
                return os << ")";
        }
    }
}