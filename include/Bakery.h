#ifndef BAKERY_H
#define BAKERY_H

#include <string>
#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <fstream>
#include <cstdlib>
#include <filesystem>
#include <functional>
#include <algorithm>

namespace bake {
    using namespace std;

    struct Token;
    class Context;
    class Variable;

    // Lexical

    enum class TokenType {
        VAR, TARGET, SET, PRINT, RUN, 
        SWAP, FILES, CONCAT, EXPAND, INPUTS, OUTPUTS, 
        ENDING, 
        NEWER, 
        FOR, IF, DO, END, WITH, ON, IN, TO,
        IDENTIFIER, STRING, 
        LPAREN, RPAREN, ASSIGN, 
        LINE
    };

    struct Token {
        TokenType type;
        string value;
    };

    class LexicalError : public exception {
        string message;
    public:
        LexicalError(int line, const string& message);

        const char* what() const noexcept;
    };

    vector<Token> Lex(istream& is);

    // General

    class RuntimeError : public exception {
        string message;
    public:
        RuntimeError(int line, const string& message);

        const char* what() const noexcept;
    };

    class Context {
        int line, t;
        vector<Token> tokens;
        unordered_map<string, Variable*> variables;
        Variable* parseValue(bool single = false);
        const function<bool(Variable*)>& parsePredicate();

        // Builtins
        void concat(Variable* target, Variable* other);
        Variable* files(Variable* folders);

        // Value manipulation.
        Variable* filterValue(Variable* var);
        void addVariable();
        void doLoop();
        void doIf();
        void setVariable();
        void print();
        void run();
        bool check();
        void processLine();
        void skipLine();
    public:
        Context(const vector<Token>& tokens);
        ~Context();

        int getLine() const;
        void process();
        void build(const string& target);
    };

    enum class Type {
        STRING, LIST, MAPPING
    };

    using ListType = vector<Variable*>;
    using MappingType = vector<pair<Variable*, Variable*>>;

    class Variable {
        Context& ctx;
        string name;
        Type type;
        void* data;
    public:
        Variable(Context& ctx, const string& value, const string& name = "");
        Variable(Context& ctx, const ListType& values, const string& name = "");
        Variable(Context& ctx, const MappingType& mappings, const string& name = "");
        Variable(Context& ctx, Type type, const string& name = "");
        Variable(const Variable& other);
        Variable(const Variable& other, const string& name);
        Variable& operator=(const Variable& other);
        ~Variable();

        bool isTemporary() const;
        const string& getName() const;
        Type getType() const;
        const string& getString() const;
        string& getString();
        const ListType& getList() const;
        ListType& getList();
        const MappingType& getMap() const;
        MappingType& getMap();
    };

    ostream& operator<<(ostream& os, const Variable& var);
}

#endif
